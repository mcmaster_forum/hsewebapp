/*
Uses the Passport.js authentication library, and the Sequelize ORM.  
See http://www.passportjs.org/ and http://docs.sequelizejs.com/ 

There is a known error issue with sequelize.  Please use these specific versions of sequelize and sequelize-cli. 
Run 'npm install --save sequelize@4.38.0' and 'npm install --save sequelize-cli@3.1.0'.  See comment by 'alexpmorris' dated 2017/11/02 at https://github.com/sequelize/sequelize/issues/7977 . 
note2Self & note2OtherDevs by Ryan



note2Self: 
*********
Making DB connection notes here, since I can't do comments in the associated config.JSON file.
Use the supplied supporting SQL text file to create the initial PostgreSQL database and DB user.  Sequelize ORM will create the tables and fields.
Database name is "hsessedb" and DB user is "hsesseuser".
Consider using .env file at web root to store the DB credentials in.  If so, remember to add .env to the .gitnore file, so credentials don't get accidentally posted to GitLab!!!
Put this same note on the readMe file.  
Confirm with Kaelan and Patrick where to store the DB password, AND also email it to Kaelan.

*/


//load bcrypt
var bCrypt = require('bcrypt-nodejs');

module.exports = function(passport,user){

var User = user;
var LocalStrategy = require('passport-local').Strategy;


passport.serializeUser(function(user, done) {
        done(null, user.id);
    });


// used to deserialize the user
passport.deserializeUser(function(id, done) {
    User.findById(id).then(function(user) {
      if(user){
        done(null, user.get());
      }
      else{
        done(user.errors,null);
      }
    });

});


passport.use('local-signup', new LocalStrategy(

  {           
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
  },

  function(req, email, password, done){   
    var generateHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
    };

     User.findOne({where: {email:email}}).then(function(user){

    if(user)
    {
      return done(null, false, {message : 'That email is already taken'} );
    }

    else
    {
      var userPassword = generateHash(password);
      var data =
      { email:email,
      password:userPassword,
      firstname: req.body.firstname,
      lastname: req.body.lastname
      };


      User.create(data).then(function(newUser,created){
        if(!newUser){
          return done(null,false);
        }

        if(newUser){
          return done(null,newUser);            
        }
      });
    }
  }); 
}
));
  
//LOCAL SIGNIN
passport.use('local-signin', new LocalStrategy(
  
{

// by default, local strategy uses username and password, we will override with email
usernameField : 'email',
passwordField : 'password',
passReqToCallback : true // allows us to pass back the entire request to the callback
},

function(req, email, password, done) {
  var User = user;
  var isValidPassword = function(userpass,password){
    return bCrypt.compareSync(password, userpass);
  }

  User.findOne({ where : { email: email}}).then(function (user) {
    if (!user) {
      return done(null, false, { message: 'Email does not exist' });
    }

    if (!isValidPassword(user.password,password)) {
      return done(null, false, { message: 'Incorrect password.' });
    }

    var userinfo = user.get();
    return done(null,userinfo);

  }).catch(function(err){
    console.log("Error:",err);
    return done(null, false, { message: 'Something went wrong with your Signin' });
  });
}
));
}

