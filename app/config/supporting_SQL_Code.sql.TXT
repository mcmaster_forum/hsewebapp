-- Supporting SQL Code

-- STEP 01 - Create Database User
CREATE ROLE "hsesseuser" WITH
	NOLOGIN
	NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'xxxxxx';
	-- Change that password of course!!


-- STEP 02 
CREATE DATABASE "hsessedb" 
  WITH OWNER = hsesseuser ENCODING = 'UTF8' CONNECTION LIMIT = -1;

  
-- STEP 03 - Web application Login
-- Log into the application via the web form.  Use the signup page to register a new user. 
  
-- STEP 04 - Test the DB   
-- Only after Sequelize ORM has created AND populated the fields from the login web form.
use hsessedb;  
SELECT * FROM public.users 
ORDER BY 1d ASC LIMIT 100;



