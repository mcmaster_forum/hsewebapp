// require express
var express = require('express');
var path    = require('path');

// create our router object
var router = express.Router();

// export our router
module.exports = router;

// route for our homepage
router.get('/', function(req, res) {
  res.render('home');
});

// route for non-authenticated pages
router.get('/search', function(req, res) {
  res.render('search');
});

router.get('/about', function(req, res) {
  res.render('pages/about');
});

router.get('/contact', function(req, res) {
  res.render('pages/contact');
});

router.get('/services', function(req, res) {
  res.render('pages/services');
});

router.post('/contact', function(req, res) {
  res.send('Thanks for contacting us, ' + req.body.name + '! We will respond shortly!');
});



// other authenticated pages  (Note2Self: fix this!!)
router.get('/pg_quotes_lombardi', function(req, res) {
  res.render('pages/pg_quotes_lombardi');
});

router.get('/pg_quotes_yogiberra', function(req, res) {
  res.render('pages/pg_quotes_yogiberra');
});

router.get('/pg_palindromes', function(req, res) {
  res.render('pages/pg_palindromes');
});
	
	
	