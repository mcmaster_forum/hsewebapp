    // require our dependencies
	const express 	= require('express');
	const path		= require('path');
	const PORT 		= process.env.PORT || 5000;
	const webPort	= PORT;
	const siteName	= "Health Systems Exchange";
	
	var expressLayouts = require('express-ejs-layouts');
	// var exphbs     	= require('express-handlebars');
	var bodyParser 	= require('body-parser');
	var app        	= express();
	var passport   	= require('passport');
	var session    	= require('express-session');
	var env        	= require('dotenv').load();

	// use ejs and express layouts
	app.set('view engine', 'ejs');
	app.use(expressLayouts);

    //For BodyParser
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());  // note2Self: check this


     // For Passport authentication
    app.use(session({ secret: 'keyboard cat',resave: true, saveUninitialized:true})); // session secret
    app.use(passport.initialize());
    app.use(passport.session()); // persist login sessions


     // For Templating - set static files (css and images, etc) location
    app.set('views', './views')
    app.use(express.static(__dirname + '/public'));

    // use handlebars templating
    // app.engine('hbs', exphbs({extname: '.hbs'}));
    // app.set('view engine', '.hbs');
    

	// Routing
	/*
	app.get('/', function(req, res){
	  res.send('Welcome to Passport with Sequelize');
	});	
	*/
	
	var authRoute 	= require('./routes/auth.js')(app,passport);
	var router 		= require('./routes/routes.js');
	app.use('/', router);	// home page

	

	// Models
    var models = require("./models");   // note2Self: move this to app sub-folder
    // var models = require("./app/models");  

    // load passport strategies
    require('./config/passport/passport.js')(passport,models.user);   // note2Self: move this to app sub-folder
    // require('./app/config/passport/passport.js')(passport,models.user);


    // Connect & sync our database
    models.sequelize.sync().then(function(){
      console.log('Excellent! Our database and DB connection looks fine. Onwards and upwards!!')
    }).catch(function(err){
      console.log(err,"Oh, no! Something went wrong with the Database Update!")
    }); 

	// Start our app
	app.listen(PORT, function(err){
		if(!err)
		console.log(`HSE - ${siteName} now started and available on port ${webPort} at http://localhost:${webPort}/ Enjoy!`);
		else console.log(err)
	});

